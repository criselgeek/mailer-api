# Requerimentos

## PHP
- Versión: 7.0 o mayor
- Mysql

## Composer components
- phpmailer/phpmailer
- Symfony/Yaml
- mailchimp/marketing

# Variables reservadas
- `$_config` (global)

# Referencias
- [Mailchimp API Reference](https://mailchimp.com/developer/marketing/api/)

# Implementación

1. Clonar el repositorio
2. Crear archivo __config.yml__ en el mismo directorio donde se clono el repositorio (No dentro de este). Se puede copiar el archivo __config-sample.yml__ dentro del directorio __example__, luego se actualizan los valores a los requeridos
3. Para crear nuevos módulos se debe crear un directorio nuevo llamado __api__, se puede tomar como referencia el ubicado dentro del directorio __example__ al igual que los directorios y archivos dentro de este para crear nuevas integraciones.
4. Para conectar el repositorio y sea funcional se debe tomar como referencia el directorio __public__ que se encuentra dentro del directorio __example__
