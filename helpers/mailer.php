<?php
namespace App\Helpers;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception as MailerException;

abstract class Mailer {
	static public function Send($data) {
		global $_config;
		$mail = new PHPMailer(true);
		try {
			//Server settings
			$mail->SMTPDebug = $_config['mailer']['debug'];                      //Enable verbose debug output
			if($_config['mailer']['smtp']) {
				$mail->isSMTP();                                            //Send using SMTP
			}
			$mail->Host       = $_config['mailer']['host'];                     //Set the SMTP server to send through
			$mail->SMTPAuth   = $_config['mailer']['smtp_auth'];                                   //Enable SMTP authentication
			$mail->Username   = $_config['mailer']['user'];                     //SMTP username
			$mail->Password   = $_config['mailer']['password'];                               //SMTP password
			$mail->SMTPSecure = $_config['mailer']['smtp_secure'];         //Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
			$mail->Port       = $_config['mailer']['port'];                                    //TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
			$mail->Timeout	=	15; // set the timeout (seconds)
			$mail->CharSet = 'UTF-8';

			//Recipients
			$mail->setFrom($_config['mailer']['sender'], $_config['mailer']['sender_name']);
			$mail->addReplyTo($data['reply_to']['email'], $data['reply_to']['name']);
			foreach($data['to'] as $to) {
				$mail->addAddress($to['email'], $to['name']);     //Add a recipient
			}
			if($data['cc'] && count($data['cc']) > 0) {
				foreach($data['cc'] as $cc) {
					$mail->addCC($cc['email'], $cc['name']);
				}
			}
			if($data['bcc'] && count($data['bcc']) > 0) {
				foreach($data['bcc'] as $bcc) {
					$mail->addBCC($bcc['email'], $bcc['name']);
				}
			}
			if($data['attachments'] && is_object($data['attachments'])) {
				foreach($data['attachments'] as $attach) {
					foreach($attach as $doc) {
						$mail->addAttachment(STATICS_DIR .$doc->file, $doc->name);
					}
				}
			}

			//Content
			$mail->isHTML(true);                                  //Set email format to HTML
			$mail->Subject = $data['subject'];
			$mail->Body    = $data['body'];
			$mail->AltBody = $data['alt_body'];

			$mail->send();
		} catch (Exception $e) {
				throw new \Exception('Message could not be sent. Mailer error ' .$mail->ErrorInfo, 1);
		}
	}
}
?>
